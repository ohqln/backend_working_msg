﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProjektAPI.Dtos
{
    public class MessageToCreate
    {
        public int IdConversation { get; set; }
        public int IdUser { get; set; }
        public string Text { get; set; }

    }
}
