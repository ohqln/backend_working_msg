﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebProjektAPI.Dtos
{
    public class UserForRegister
    {
        [Required]
        public string Email { get; set; }
        [Required]
        // [StringLength(15, MinimumLength = 6, ErrorMessage = "Heslo musí mít minimálně 6 znaků")]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }

    }
}
