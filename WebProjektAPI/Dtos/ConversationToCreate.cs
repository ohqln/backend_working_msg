﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProjektAPI.Dtos
{
    public class ConversationToCreate
    {
        public bool IsGroup { get; set; }
        public byte[] Photo { get; set; }
        public string Name { get; set; }
        public int IdUser { get; set; }
    }
}
