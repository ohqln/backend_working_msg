﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProjektAPI.Models
{
    public class Message
    {
        public int Id { get; set; }
        public int IdConversation { get; set; }
        public int SentBy { get; set; }
        public string Text { get; set; }
        public DateTime MessageTime { get; set; }
      
    }
}
