﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProjektAPI.Models
{
    public class UserConversation
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdConversation { get; set; }
    }
}
