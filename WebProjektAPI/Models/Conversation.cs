﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProjektAPI.Models
{
    public class Conversation
    {
        public int Id { get; set; }
        public bool IsGroup { get; set; }
        public byte[] Photo { get; set; }
        public string Name { get; set; }
    }
}
