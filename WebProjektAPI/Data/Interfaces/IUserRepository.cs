﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data.Interfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<Users>> GetUsers();
        //Task<IEnumerable<Users>> GetUsersBySchool(int idSchool);
        //Task<IEnumerable<Users>> GetUsersByGroup(int idGroup);
        Task<Users> GetUserById(int id);
    }
}
