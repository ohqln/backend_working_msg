﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data.Interfaces
{
    public interface IMessageRepository
    {
        Task<IEnumerable<Message>> GetMessages();
        Task<IEnumerable<Message>> GetMessagesByIdConversation(int idConversation);
        Task<IEnumerable<Message>> GetMessagesByIdConversationAndUser(int idConversation, int idUser);
        Task<Message> Create(Message message);
        //Task<Messages> GetMessageByIdUser(int idUser);
    }
}
