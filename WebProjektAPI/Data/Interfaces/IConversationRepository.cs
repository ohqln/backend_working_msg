﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data.Interfaces
{
    public interface IConversationRepository
    {
        Task<IEnumerable<Conversation>> GetConversation(int idUser);
        Task<Conversation> GetConversationById(int id);
        Task<Conversation> Create(Conversation conversation);
        Task<UserConversation> CreateUserConversation(UserConversation userConversation);
    }
}
