﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Data.Interfaces;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        public DataContext Context;
        public UserRepository(DataContext context)
        {
            this.Context = context;
        }
        public async Task<Users> GetUserById(int id)
        {
            var user = await this.Context.Users.FirstOrDefaultAsync(s => s.Id == id);
            return user;
        }

        public async Task<IEnumerable<Users>> GetUsers()
        {
            var users = await this.Context.Users.ToListAsync();
            return users;
        }

        //public async Task<IEnumerable<Users>> GetUsersByGroup(int idGroup)
        //{
        //    var group = await this.Context.UsersGroups.FirstOrDefaultAsync(s => s.IdSGroup == idGroup);
        //    var users = await this.Context.Users.Where(s => s.Id == group.IdUser).ToListAsync();
        //    return users;
        //}

        //public async Task<IEnumerable<Users>> GetUsersBySchool(int idSchool)
        //{
        //    var school = await this.Context.UsersSchools.FirstOrDefaultAsync(s => s.IdSchool == idSchool);
        //    var users = await this.Context.Users.Where(s => s.Id == school.IdUser).ToListAsync();
        //    return users;

        //}
        //GetUsersByGroup() !!!!
    }
}
