﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Data.Interfaces;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data.Repositories
{
    public class ConversationRepository : IConversationRepository
    {

        public DataContext Context;
        public ConversationRepository(DataContext context)
        {
            this.Context = context;
        }

        public async Task<Conversation> Create(Conversation conversation)
        {
            await this.Context.Conversation.AddAsync(conversation);
            
            await this.Context.SaveChangesAsync();

            return conversation;
        }
        public async Task<UserConversation> CreateUserConversation(UserConversation userConversation)
        {
            await this.Context.UserConversation.AddAsync(userConversation);
            await this.Context.SaveChangesAsync();

            return userConversation;
        }

        public async Task<IEnumerable<Conversation>> GetConversation(int idUser)
        {
            var uConv = await this.Context.UserConversation.FirstOrDefaultAsync(s => s.IdUser == idUser);
            var conv = await this.Context.Conversation
                .Where(x => x.Id == uConv.IdConversation)
                .ToListAsync();
            return conv;
        }

        public async Task<Conversation> GetConversationById(int id)
        {
            var conv = await this.Context.Conversation.FirstOrDefaultAsync(s => s.Id == id);
            return conv;
        }
    }
}
