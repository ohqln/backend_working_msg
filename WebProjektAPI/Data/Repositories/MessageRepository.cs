﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Data.Interfaces;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        public DataContext Context;
        public MessageRepository(DataContext context)
        {
            this.Context = context;
        }

        public async Task<Message> Create(Message message)
        {
            await this.Context.Message.AddAsync(message);

            await this.Context.SaveChangesAsync();

            return message;
        }

        public async Task<IEnumerable<Message>> GetMessages()
        {
            var message = await this.Context.Message.ToListAsync();
            return message;
        }

        //public async Task<Messages> GetMessageByIdConversation(int id)
        //{
        //    var message = await this.Context.Messages.FirstOrDefaultAsync(s => s.Id == id);
        //    return message;
        //}
        public async Task<IEnumerable<Message>> GetMessagesByIdConversation(int idConversation)
        {
            var message = await this.Context.Message
                .Where(m => m.IdConversation == idConversation)
                .ToListAsync();
            return message;
        }
        public async Task<IEnumerable<Message>> GetMessagesByIdConversationAndUser(int idConversation, int idUser)
        {
            var message = await this.Context.Message
                .Where(m => m.IdConversation == idConversation)
                .Where(m => m.SentBy == idUser)
                .ToListAsync();
            return message;
        }
    }
}
