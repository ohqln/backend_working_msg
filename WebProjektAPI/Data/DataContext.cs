﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebProjektAPI.Models;

namespace WebProjektAPI.Data
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Message> Message { get; set; }

        public DbSet<Conversation> Conversation { get; set; }
        public DbSet<UserConversation> UserConversation { get; set; }

        //public DbSet<Test> Test { get; set; }
        //public DbSet<Question> Question { get; set; }
        //public DbSet<Answer> Answer { get; set; }
        //public DbSet<School> School { get; set; }
        //public DbSet<UsersSchools> UsersSchools { get; set; }
        //public DbSet<Groups> Groups { get; set; }
        //public DbSet<UsersGroups> UsersGroups { get; set; }
        //public DbSet<TestRecord> TestRecord { get; set; }
    }
}
