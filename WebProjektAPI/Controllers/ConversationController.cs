﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebProjektAPI.Data.Interfaces;
using WebProjektAPI.Dtos;
using WebProjektAPI.Models;

namespace WebProjektAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    //public class ConversationController : ControllerBase
    public class ConversationController : ControllerBase
    {
        private readonly IConversationRepository Repo;
        private readonly IMessageRepository MsgRepo;
        public ConversationController(IConversationRepository repo, IMessageRepository msgRepo)
        {
            this.Repo = repo;
            this.MsgRepo = msgRepo;
        }
        [HttpGet]
        public async Task<IActionResult> GetConversations()
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var converations = await this.Repo.GetConversation(Convert.ToInt32(userId));
            return Ok(converations);
        }
        [HttpPost("create")]
        public async Task<IActionResult> Create(ConversationToCreate ctc)
        {

            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;  // User that is logged in and is creating a test
            Conversation NewConversation = new Conversation
            {
                IsGroup = ctc.IsGroup,
                Name = ctc.Name,
                Photo = ctc.Photo
                
            };
            Conversation CreatedConversation = await this.Repo.Create(NewConversation);
            UserConversation NewUserConversation = new UserConversation
            {
                IdConversation = CreatedConversation.Id,
                IdUser = Convert.ToInt32(userId)
                
            };
            UserConversation CreatedUserConversation = await this.Repo.CreateUserConversation(NewUserConversation);
            UserConversation NewUserConversation2 = new UserConversation
            {
                IdConversation = CreatedConversation.Id,
                IdUser = ctc.IdUser
            };
            UserConversation CreatedUserConversation2 = await this.Repo.CreateUserConversation(NewUserConversation2);

            return StatusCode(201);
        }
        [HttpGet("messages")]
        public async Task<IActionResult> GetMessageByIdConversation(int idConversation)
        {
            var msg = await this.MsgRepo.GetMessagesByIdConversation(idConversation);

            return Ok(msg);
        }
        [HttpGet("messagesByLoggedUser")]
        public async Task<IActionResult> GetMessageByIdConversationLoggedUser(int idConversation)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var msg = await this.MsgRepo.GetMessagesByIdConversationAndUser(idConversation, Convert.ToInt32(userId));

            return Ok(msg);
        }
        [HttpGet("messagesByOtherUser")]
        public async Task<IActionResult> GetMessageByIdConversationLoggedUser(int idConversation, int idUser)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var msg = await this.MsgRepo.GetMessagesByIdConversationAndUser(idConversation, idUser);

            return Ok(msg);
        }
        [HttpPost("createMessage")]
        public async Task<IActionResult> CreateMessage(MessageToCreate mtc)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;  // User that is logged in and is creating a test
            Message NewMessage = new Message
            {
                IdConversation = mtc.IdConversation,
                SentBy = Convert.ToInt32(userId),
                Text = mtc.Text,
                MessageTime = DateTime.Now

            };
            Message CreatedMessage = await this.MsgRepo.Create(NewMessage);

            return StatusCode(201);
        }

            //[HttpGet("{id}")]
            //public async Task<IActionResult> GetUserById(int id)
            //{
            //    var user = await this.Repo.GetUserById(id);

            //    return Ok(user);
            //}

        }
    }
