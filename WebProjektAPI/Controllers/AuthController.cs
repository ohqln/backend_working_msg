﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Cors;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebProjektAPI.Data;
using WebProjektAPI.Dtos;
using WebProjektAPI.Models;

namespace WebProjektAPI.Controllers
{
    // [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository Repo;
        private readonly IConfiguration Config;
        public AuthController(IAuthRepository repo, IConfiguration config)
        {
            this.Repo = repo;
            this.Config = config;
        }
        [HttpPost("register")]
        //[HttpOptions]
        public async Task<IActionResult> Register(UserForRegister ufr)
        {
            //if (this.HttpContext.Request.Method == "OPTIONS")
            //{
            //    Response.Headers.Add("Access-Control-Allow-Origin", "*");
            //    Response.Headers.Add("Access-Control-Allow-Headers", "*");
            //    return Ok();

            //}


            ufr.Email = ufr.Email.ToLower();
            if (await Repo.USerExists(ufr.Email))
                return BadRequest("User already exists");
            if (ufr.Password.Length < 6) {
                return BadRequest("Password is too short, it must have more than 6 characters");
            }
            Users NewUser = new Users
            {
                Email = ufr.Email,
                Name = ufr.Name,
                Surname = ufr.Surname,
                AccountType = "user"
            };
            Users CreatedUSer = await Repo.Register(NewUser, ufr.Password);
            return StatusCode(201);
        }
        // [EnableCors("AllowOrigin")]
        [HttpPost("login")]
        //[HttpOptions]
        public async Task<IActionResult> Login(UserForLogin ufl)
        {

            //if (this.HttpContext.Request.Method == "OPTIONS")
            //{
            //    Response.Headers.Add("Access-Control-Allow-Origin", "*");
            //    Response.Headers.Add("Access-Control-Allow-Headers", "*");
            //    return Ok();

            //}


            //Response.Headers.Add("Access-Control-Allow-Origin", "*");

            Users ExistingUser = await Repo.Login(ufl.Email.ToLower(), ufl.Password);

            if (ExistingUser == null)
                return Unauthorized();

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, ExistingUser.Id.ToString()),
                new Claim(ClaimTypes.Name, ExistingUser.Email)
               , new Claim(ClaimTypes.Role, ExistingUser.AccountType)
               , new Claim(ClaimTypes.GivenName, ExistingUser.Name)
               , new Claim(ClaimTypes.Surname, ExistingUser.Surname)
               // , new Claim(ClaimTypes.)
              
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.Config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var TokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(2),
                SigningCredentials = creds,                 
                
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(TokenDescriptor);

            return Ok(new
            {
                token = tokenHandler.WriteToken(token)
            });


        }
    }
}
